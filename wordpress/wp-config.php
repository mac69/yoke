<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'eleos');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'D;e6Ozku%$X.L{{qBsRo8xv})u{=QPpY-)@m`&<mTM&5^VlR)$HEF?cP,pR@I;m ');
define('SECURE_AUTH_KEY',  ' <c|&Nr_-Fg!&w^nB1$pSeW&gvx&f6_v{l)c(MW>q*r`;2c,%3{!<6VccQ<#nn2g');
define('LOGGED_IN_KEY',    ',EsgL}Ky^dgF`Z`}%QvT5spH]U}<-5y:KGQ|JHwx_WhE-8<6f5C@ufS8Wu/gvNo|');
define('NONCE_KEY',        'VxbM=MJv7LAiGh*QPg>Eeb{N=poQ!HhOOIa@)G-7&!}mH)f,z6{LI;Wb^aUyMeNJ');
define('AUTH_SALT',        'nfCL*O%FBtxWf$/Ic]{Jqv.+*Dj?&=/g4=<q3H:IVVfGH`3Xc^6a>0_N6hC$]Or9');
define('SECURE_AUTH_SALT', '+punuV!udjw9)z9#/J*MNB$Y)2Tm;%J3E_fo/un%ly^inu<>:3;?;wXk1Mj[)*hg');
define('LOGGED_IN_SALT',   'uSX*lI>Rml241/aQzgf7hDiG[K[#4J!d|PO*;>{{1,Jy.i)$e-;As]0i1(VnT^c.');
define('NONCE_SALT',       '7e`bv=y2D!9GDJ?< 5d1&m4T|YDzhu~ga^w>Z}#DMQ+hE:iU8$apcEdzQ`o(Bxx,');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
